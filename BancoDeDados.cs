
using Microsoft.EntityFrameworkCore;

public class BancoDeDados : DbContext
{

    //Configuração do banco de dados MySql
    protected override void OnConfiguring(DbContextOptionsBuilder builder)
    {
        builder.UseMySQL("server=localhost;port=3306;database=restaurante;" +
         "user=root;password=positivo");
    }


    //Tabelas do banco de dados
    public DbSet<Pessoa> Pessoas { get; set; }
    public DbSet<Produto> Produtos { get; set; }
    //...

}